package com.nurdicky.bt_arduinohc05;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnOtpCompletionListener{

    private static final UUID mUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    static final String TAG = "BT-HC05";
    private static final int REQUEST_ENABLE_BT = 1;
    private static final String BT_ADDRESS = "00:18:91:D8:0E:5D";

    private BluetoothService mBTService;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread; // bluetooth background worker thread to send and receive data
    private BluetoothSocket mBTSocket = null; // bi-directional client-to-client data path

    private ArrayList<DeviceItem> deviceItemList = new ArrayList<DeviceItem>();
    private BluetoothAdapter mBTAdapter;
    private int _BT_STATUS = 0;

    private OtpView otpView;
    private Button validateButton;
    private TextView vBtStatus;
    private TextView vBtConnect;
    private TextView vGetCode;
    private TextView mReadBuffer;
    private Handler mHandler;

    private final static int MESSAGE_READ = 2; // used in bluetooth handler to identify message update
    private final static int CONNECTING_STATUS = 3; // used in bluetooth handler to identify message status
    private boolean doubleBackToExitPressedOnce = false;

    private int mState;
    private int mNewState;

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUi();
        setListeners();

        mBTAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBTAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }else{
            setBTstatus(1);
            pairedDevices();
        }

        mHandler = new Handler(){
            public void handleMessage(android.os.Message msg){
                Log.d(TAG, "handleMessage: " + msg);
                if(msg.what == MESSAGE_READ){
                    String readMessage = null;
                    try {
                        Log.d(TAG, "read obj: " + msg.obj);
                        readMessage = new String((byte[]) msg.obj, "UTF-8");
                        otpView.setText(readMessage);
                        Log.d(TAG, "readMessage : " + readMessage.toUpperCase() );
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.d(TAG, "Eror handleMessage: " + e.getMessage());
                    }
                    mReadBuffer.setText(readMessage);
                }

                if(msg.what == CONNECTING_STATUS){
                    if(msg.arg1 == 1) {
                        Toast.makeText(MainActivity.this, "Connected to Device: " + (String) (msg.obj), Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(MainActivity.this, "Connection Failed", Toast.LENGTH_SHORT).show();

                        for (int i = 0; i < deviceItemList.size(); i++) {
                            String address = deviceItemList.get(i).getAddress();
                            String name = deviceItemList.get(i).getName();

                            if (BT_ADDRESS.equals(address)){
                                getDataFromBTSocket(address, name);
                            }
                        }

                    }
                }
            }
        };

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                setBTstatus(1);
                pairedDevices();

            }else{
                Toast.makeText(this, "Bluetooth doesn't active !", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            String bt_address = "00:18:91:D8:0E:5D";
            String bt_address2 = "3C:95:09:3D:9F:63";

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch(state) {
                    case BluetoothAdapter.STATE_OFF:
                        Toast.makeText(context, "Bluetooth is OFF", Toast.LENGTH_SHORT).show();
                        setBTstatus(0);
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Toast.makeText(context, "Bluetooth is ON", Toast.LENGTH_SHORT).show();
                        setBTstatus(1);
                        break;
                }
            }
            else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                //Device is now connected
                Log.d(TAG, "1 :: " + device.getAddress() + " 2 :: " + BT_ADDRESS);
                if (device.getAddress().equals(BT_ADDRESS)){
                    Log.d(TAG, "BT connecting: " + device.getName() );
                    getDataFromBTSocket(device.getAddress(), device.getName());
                }else{
                    Log.d(TAG, "BT connecting: connection not support " + device.getName() );
                }
            }
        }
    };

    private void setBTstatus(int state) {
        if (state == 1){ // ON
            vBtStatus.setText("ON");
            vBtStatus.setTextColor(Color.WHITE);
        }else{ // OFF
            vBtStatus.setText("OFF");
            vBtStatus.setTextColor(Color.RED);
        }
    }

    private void setBTConnection(int state) {
        if (state == 1){
            _BT_STATUS = 1;
            vBtConnect.setText("Connected");
            vBtConnect.setTextColor(Color.WHITE);
        }else{
            _BT_STATUS = 0;
            vBtConnect.setText("Not Connected");
            vBtConnect.setTextColor(Color.RED);
        }
    }

    private void getDataFromBTSocket(final String address, final String name) {
        // Spawn a new thread to avoid blocking the GUI one
        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading..."); // Setting Message
        pDialog.setTitle("Please waiting "); // Setting Title
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        pDialog.show(); // Display Progress Dialog
        pDialog.setCancelable(false);

        new Thread()
        {
            public void run() {
                boolean fail = false;

                BluetoothDevice device = mBTAdapter.getRemoteDevice(address);
                //connect(device);
                Log.d(TAG, "device : " + device);

                try {
                    mBTSocket = createBluetoothSocket(device);
                } catch (IOException e) {
                    fail = true;
                    Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_SHORT).show();
                }
                Log.d(TAG, "socket : " + mBTSocket);

                mBTAdapter.cancelDiscovery();
                // Establish the Bluetooth socket connection.
                try {
                    // Connect to the remote device through the socket. This call blocks
                    // until it succeeds or throws an exception.
                    mBTSocket.connect();
                } catch (IOException connectException) {
                    // Unable to connect; close the socket and return.
                    Log.d(TAG, "Connect : " + connectException );
                    try {
                        fail = true;
                        mBTSocket.close();
                        mHandler.obtainMessage(CONNECTING_STATUS, -1, -1)
                                .sendToTarget();
                        setBTConnection(0);
                    } catch (IOException closeException) {
                        Log.e(TAG, "Could not close the client socket", closeException);
                        //Toast.makeText(getBaseContext(), "Could not close the client socket", Toast.LENGTH_SHORT).show();
                    }
                }

                Log.d(TAG, "fail : " + fail);
                if(fail == false) {
                    mConnectedThread = new ConnectedThread(mBTSocket);
                    mConnectedThread.start();

                    Log.d(TAG, "thread connected : " + mConnectedThread);
                    setBTConnection(1);

                    mHandler.obtainMessage(CONNECTING_STATUS, 1, -1, name)
                            .sendToTarget();
                }

                pDialog.dismiss();

            }
        }.start();
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        try {
            final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", UUID.class);
            return (BluetoothSocket) m.invoke(device, mUUID);
        } catch (Exception e) {
            Log.e(TAG, "Could not create Insecure RFComm Connection",e);
        }
        return  device.createRfcommSocketToServiceRecord(mUUID);
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer; // mmBuffer store for the stream

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.d(TAG, "ConnectedThread Failed : " + e.getMessage() );
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            mmBuffer = new byte[1024];
            int numBytes; // bytes returned from read()

            Log.d(TAG, "buffer : " + mmBuffer );
            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    numBytes  = mmInStream.available();
                    if(numBytes  != 0) {
                        mmBuffer = new byte[1024];
                        SystemClock.sleep(100); //pause and wait for rest of data. Adjust this depending on your sending speed.
                        numBytes = mmInStream.available(); // how many bytes are ready to be read?
                        numBytes = mmInStream.read(mmBuffer, 0, numBytes); // record how many bytes we actually read
                        Log.d(TAG, "read bytes : " + numBytes);
                        mHandler.obtainMessage(MESSAGE_READ, numBytes, -1, mmBuffer)
                                .sendToTarget(); // Send the obtained bytes to the UI activity
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d(TAG, "Input stream was disconnected", e);
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String input) {
            byte[] bytes = input.getBytes();           //converts entered String into bytes
            try {
                Log.d(TAG, "write: " + bytes);
                mmOutStream.write(bytes);
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when sending data", e);
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }
    }

    private void pairedDevices(){
        Set<BluetoothDevice> pairedDevices = mBTAdapter.getBondedDevices();
        Log.d(TAG, "BT pairedDevices : " + pairedDevices );

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                DeviceItem newDevice = new DeviceItem(device.getName(),device.getAddress(), false);
                deviceItemList.add(newDevice);

                if (newDevice.getAddress().equals(BT_ADDRESS)){
                    getDataFromBTSocket(newDevice.getAddress(), newDevice.getName());
                }
            }
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initializeUi() {
        otpView         = findViewById(R.id.otp_view);
        validateButton  = findViewById(R.id.validate_button);
        vBtStatus       = findViewById(R.id.v_bt_status);
        vBtConnect      = findViewById(R.id.v_bt_connect);
        vGetCode        = findViewById(R.id.getCode);
        mReadBuffer     = findViewById(R.id.read_message);
    }

    private void setListeners() {
        validateButton.setOnClickListener(this);
        otpView.setOtpCompletionListener(this);
        vGetCode.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.validate_button:
                Toast.makeText(this, otpView.getText(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "validate click : " + mConnectedThread);
                String code = String.valueOf(otpView.getText());

                if(mConnectedThread != null) {
                    mConnectedThread.write(code);
                }else{
                    Toast.makeText(this, "Connection failed !", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.getCode:
                String address      = mBTAdapter.getAddress();
                String name         = mBTAdapter.getName();
                String bt_address   = "00:18:91:D8:0E:5D";

                if(mConnectedThread != null) {
                    mConnectedThread.run();
                }else{
                    Toast.makeText(this, "Connection failed !", Toast.LENGTH_SHORT).show();
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onOtpCompleted(String s) {
        //Toast.makeText(this, "Completed", Toast.LENGTH_SHORT).show();
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();

        if(mConnectedThread != null) {
            mConnectedThread.write(s);
        }else{
            Toast.makeText(this, "Connection failed !", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            //if (mConnectedThread != null) mConnectedThread.cancel();

            if (mBTAdapter != null) {
                mBTAdapter.cancelDiscovery();
            }

            // Unregister broadcast listeners
            unregisterReceiver(mReceiver);
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    protected void onStop()
    {
        unregisterReceiver(mReceiver);
        super.onStop();
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private String mSocketType;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket tmp = null;

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                tmp = device.createInsecureRfcommSocketToServiceRecord(mUUID);
            } catch (IOException e) {
                Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
            }
            mmSocket = tmp;
            mState = STATE_CONNECTING;
        }

        public void run() {
            // Always cancel discovery because it will slow down a connection
            mBTAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();
            } catch (IOException e) {
                // Close the socket
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    Log.e(TAG, "unable to close() " + mSocketType +
                            " socket during connection failure", e2);
                }
//                Toast.makeText(getApplicationContext(), "Unable to connect device", Toast.LENGTH_SHORT).show();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (MainActivity.this) {
                mConnectThread = null;
            }

            // Start the connected thread
            connected(mmSocket, mmDevice);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect " + mSocketType + " socket failed", e);
            }
        }
    }

    public synchronized void connect(BluetoothDevice device) {
        Log.d(TAG, "connect to: " + device);

        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        // Update UI title
        setBTConnection(1);
    }

    public synchronized void connected(BluetoothSocket socket, BluetoothDevice
            device) {
        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        mHandler.obtainMessage(CONNECTING_STATUS, 1, -1, device.getName())
                .sendToTarget();
        // Update UI title
        setBTConnection(1);
    }

}
